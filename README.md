# Unity XML Localization
This is the Simple Localization Manager based on XML Files.
You can use it on any platforms.

<b>Tested on Unity 2018.3.1</b>

## Prepare XML Files
Create your localization files and put them here:<br/>
`Assets/Game Assets/Localization/`

<b>Required XML Tags:</b><br/>
`<?xml version="1.0" encoding="utf-8"?>`<br/>
`<Language LANG="Russian" ID="1">... PUT HERE YOUR LOCALIZATION TAGS ...</Language>`

<b>Where LANG - your language name and ID - language ID (must be unique)</b>

<b>Localization Tags:</b><br/>
`<text key="KEY_NAME">Value</text>`

## Localization XML Example
`<?xml version="1.0" encoding="utf-8"?>`<br/>
`<Language LANG="Russian" ID="1">`<br/>
`  <!-- GENERAL STRINGS -->`<br/>
`  <text key="GAME_NAME">Example Game</text>`<br/>
`  <text key="TEST_VALUE">Example Value</text>`<br/>
`  <!-- GENERAL STRINGS -->`<br/>
`</Language>`

## How to Use
<b>To switch language by ID:</b><br/>
`LocalizationManager.Instance.SetLocalizationID(/* LOCALE ID (int) */);`

<b>To get localization text:</b><br/>
`LocalizationManager.Instance.GetText(/* KEY NAME (string) */);`

## Roadmap
In the next versions:
- Adding UI Components to control your data;
- Adding Auto ID system;
- Adding new events;
- Adding Audio Localization;