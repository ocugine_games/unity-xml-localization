﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;

//=================================================
//  Simple XML Localization
//  Developed by Ocugine Entertainment
//
//  Version:        0.5.3
//  Build:          503
//  Developer:      Elijah Rastorguev
//  URL:            https://gitlab.com/ocugine_games/
//=================================================
//  Localization Manager Class
//=================================================
public class LocalizationManager : MonoBehaviour{
    // Public Parameters
    public static LocalizationManager Instance { get { return instance; } } // Instance
    public int currentLanguageID = 0; // Current Language

    [SerializeField]
    public List<TextAsset> languageFiles = new List<TextAsset>(); // Language Files
    public List<Language> languages = new List<Language>(); // Languages

    // Private Parameters
    private static LocalizationManager instance;   // GameSystem local instance

    // Before Game Started
    void Awake(){
        // Set Instance
        instance = this;
        DontDestroyOnLoad(this);

        // This will read  each XML file from the languageFiles list<> and populate the languages list with the data
        foreach (TextAsset languageFile in languageFiles){
            XDocument languageXMLData = XDocument.Parse(languageFile.text); // Parse XML
            Language language = new Language(); // Create Language
            language.languageID = System.Int32.Parse(languageXMLData.Element("Language").Attribute("ID").Value); // SEt ID
            language.languageString = languageXMLData.Element("Language").Attribute("LANG").Value; // Set String

            // Fill List of Values
            foreach (XElement textx in languageXMLData.Element("Language").Elements()){
                TextKeyValue textKeyValue = new TextKeyValue();
                textKeyValue.key = textx.Attribute("key").Value;
                textKeyValue.value = textx.Value;
                language.textKeyValueList.Add(textKeyValue);
            }

            // Add Language
            languages.Add(language);
        }
    }

    // GetText will go through each language in the languages list and return a string matching the key provided 
    public string GetText(string key){
        // For Every Language
        foreach (Language language in languages){

            // Current Language
            if (language.languageID == currentLanguageID){
                foreach (TextKeyValue textKeyValue in language.textKeyValueList){
                    if (textKeyValue.key == key){
                        return textKeyValue.value;
                    }
                }
            }
        }

        // Return
        return "Undefined";
    }

    // Set Another Localization ID
    public delegate void LocaliziationIDChanged();
    public event LocaliziationIDChanged OnLocaleChanged;
    public void SetLocalizationID(int id){
        currentLanguageID = id; // Set ID
        if (OnLocaleChanged != null) OnLocaleChanged(); // Call Event
    }
}
// Simple Class to hold the language metadata
[System.Serializable]
public class Language{
    public string languageString; // Language String
    public int languageID; // Language ID
    public List<TextKeyValue> textKeyValueList = new List<TextKeyValue>(); // Text Key Value List
}
// Simple class to hold the key/value pair data
[System.Serializable]
public class TextKeyValue{
    public string key; // Key
    public string value; // Value
}